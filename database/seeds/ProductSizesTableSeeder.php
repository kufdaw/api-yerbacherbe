<?php

use Illuminate\Database\Seeder;

class ProductSizesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_sizes')->insert([
            ['id' => 1, 'product_id' => 1, 'name' => 'small', 'volume' => 300, 'price' => 800],
            ['id' => 2, 'product_id' => 1, 'name' => 'normal', 'volume' => 400, 'price' => 1000],
            ['id' => 3, 'product_id' => 1, 'name' => 'big', 'volume' => 500, 'price' => 1200],
            ['id' => 4, 'product_id' => 2, 'name' => 'small', 'volume' => 300, 'price' => 800],
            ['id' => 5, 'product_id' => 2, 'name' => 'normal', 'volume' => 400, 'price' => 1000],
            ['id' => 6, 'product_id' => 2, 'name' => 'big', 'volume' => 500, 'price' => 1200],
            ['id' => 7, 'product_id' => 3, 'name' => 'small', 'volume' => 300, 'price' => 800],
            ['id' => 8, 'product_id' => 3, 'name' => 'normal', 'volume' => 400, 'price' => 1000],
            ['id' => 9,  'product_id' => 3, 'name' => 'big', 'volume' => 500, 'price' => 1200]
        ]);
    }
}
