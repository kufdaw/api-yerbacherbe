<?php

use Illuminate\Database\Seeder;

class ShopsManagersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shops_managers')->insert([
            ['shop_id' => 1, 'user_id' => 1],
            ['shop_id' => 2, 'user_id' => 1],
            ['shop_id' => 3, 'user_id' => 2],
        ]);
    }
}
