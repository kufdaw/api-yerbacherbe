<?php

use Illuminate\Database\Seeder;

class OrderItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_items')->insert([
            ['order_id' => 1, 'product_id' => 1, 'product_size_id' => 1, 'amount' => 2],
            ['order_id' => 1, 'product_id' => 1, 'product_size_id' => 2, 'amount' => 1],
            ['order_id' => 1, 'product_id' => 3, 'product_size_id' => 9, 'amount' => 4],
            ['order_id' => 2, 'product_id' => 1, 'product_size_id' => 1, 'amount' => 1],
            ['order_id' => 3, 'product_id' => 2, 'product_size_id' => 6, 'amount' => 1],
            ['order_id' => 3, 'product_id' => 3, 'product_size_id' => 9, 'amount' => 2],
            ['order_id' => 4, 'product_id' => 1, 'product_size_id' => 2, 'amount' => 2],
            ['order_id' => 4, 'product_id' => 1, 'product_size_id' => 3, 'amount' => 3],
            ['order_id' => 5, 'product_id' => 1, 'product_size_id' => 1, 'amount' => 1],
        ]);
    }
}