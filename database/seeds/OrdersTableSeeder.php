<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            ['id' => 1, 'seller_id' => 2, 'shop_id' => 1, 'status' => 'received', 'price' => 3800],
            ['id' => 2, 'seller_id' => 2, 'shop_id' => 2, 'status' => 'taken', 'price ' => 800],
            ['id' => 3, 'seller_id' => 2, 'shop_id' => 1, 'status' => 'in-progress', 'price' => 2000],
            ['id' => 4, 'seller_id' => 2, 'shop_id' => 2, 'status' => 'ready', 'price' => 4400],
            ['id' => 5, 'seller_id' => 2, 'shop_id' => 1, 'status' => 'received', 'price' => 800],
        ]);
    }
}
