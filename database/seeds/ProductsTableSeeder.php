<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            ['id' => 1, 'name' => 'Yerba', 'desc' => 'Yerba Mate, Herbata Oolong, Dzika Róża, Truskawki, Wanilia'],
            ['id' => 2, 'name' => 'Kawa', 'desc' => 'Świeżo prażone ziarna kawy arabskiej, aromat francuskiej wanilii'],
            ['id' => 3, 'name' => 'Herba', 'desc' => 'Pu-Erh, Malina, Jeżyna, Truskawka, Hibiskus, Jabłko, Dzika róża, Mango, Ananas, Papaja']
        ]);
    }
}
