<?php

use Illuminate\Database\Seeder;

class ShopsSellersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shops_sellers')->insert([
            ['shop_id' => 1, 'user_id' => 2],
            ['shop_id' => 1, 'user_id' => 3],
            ['shop_id' => 2, 'user_id' => 1],
            ['shop_id' => 2, 'user_id' => 2]
        ]);
    }
}
