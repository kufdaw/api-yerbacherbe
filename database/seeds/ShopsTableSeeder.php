<?php

use Illuminate\Database\Seeder;

class ShopsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shops')->insert([
            ['name' => 'Yerbać Herbę Jana Pawla', 'coordinate_x' => 14.98765, 'coordinate_y' => 45.98765, 'street' => 'jana pawla 12', 'city' => 'warszawa', 'postal_code' => '87-654'],
            ['name' => 'Yerbać Herbę Teatr Słowackiego', 'coordinate_x' => 14.98765, 'coordinate_y' => 45.98765, 'street' => 'basztowa 74', 'city' => 'krakow', 'postal_code' => '87-654'],
            ['name' => 'Yerbać Herbę Plac Inwalidow', 'coordinate_x' => 14.98765, 'coordinate_y' => 45.98765, 'street' => 'Słowackiego 149', 'city' => 'krakow', 'postal_code' => '87-654'],
            ['name' => 'Yerbać Herbę Puławska', 'coordinate_x' => 23.98456, 'coordinate_y' => 98.34765, 'street' => 'pulawska 76', 'city' => 'krakow', 'postal_code' => '56-987']
        ]);
    }
}
