<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['name' => 'Dawid Kufel', 'email' => 'dawid@kufel.pl', 'role' => 'admin', 'password' => bcrypt('test123')],
            ['name' => 'Maciej Łuczka', 'email' => 'maciek@luczka.pl', 'role' => 'seller', 'password' => bcrypt('test123')],
            ['name' => 'Mateusz Niziołek', 'email' => 'ma@tluszcz.pl', 'role' => 'seller', 'password' => bcrypt('test123')]
        ]);
    }
}