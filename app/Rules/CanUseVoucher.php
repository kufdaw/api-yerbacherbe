<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CanUseVoucher implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($value === 'free' && (auth()->user()->vouchersUsedToday->count() >= 3 || count(request()->products) !== 1 || request()->products[0]['amount'] !== 1)) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'You have used already your vouchers.';
    }
}
