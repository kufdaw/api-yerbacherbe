<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class AllowedDayDifference implements Rule
{

    protected $allowedDifferenceDays;

    /**
     * Create a new rule instance.
     *
     * @param int $allowedDifferenceDays
     */
    public function __construct(int $allowedDifferenceDays)
    {
        $this->allowedDifferenceDays = $allowedDifferenceDays;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $startDate = Carbon::createFromFormat('m-d-Y', request()->query('start_date'))->startOfDay();
        $endDate = Carbon::createFromFormat('m-d-Y', request()->query('end_date'))->startOfDay();

        return $this->checkIfAllowedDayDifference($startDate, $endDate, $this->allowedDifferenceDays);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Too wide date selection range.';
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param int $daysAmount
     * @return bool
     */
    public function checkIfAllowedDayDifference($startDate, $endDate, int $daysAmount): bool
    {
        return !($startDate->diffInDays($endDate) > $daysAmount);
    }
}
