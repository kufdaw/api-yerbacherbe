<?php

namespace App\Http\Repositories;

use App\Models\Order;
use App\Models\Shop;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class OrderRepository
{
    /**
     * @param Shop $shop
     * @return Collection
     */
    public function indexActive(Shop $shop): Collection
	{
		return $shop->orders()->whereIn('status', ['received', 'in-progress', 'ready'])->with('products')->get();
	}

    /**
     * @param Shop $shop
     * @param $startDate
     * @param $endDate
     * @return Collection
     */
    public function indexHistory(Shop $shop, $startDate, $endDate): Collection
	{
		return $shop->orders()->where('status', 'taken')->with('products')->orderBy('created_at', 'desc')->where('created_at', '>', $startDate)->where('created_at', '<', $endDate)->get();
	}

    /**
     * @param Shop $shop
     * @param $data
     * @param $totalOrderPrice
     * @param $productsPreparedToAttach
     * @return Model
     */
    public function create(Shop $shop, $data, $totalOrderPrice, $productsPreparedToAttach): Model
    {
		$order = $shop->orders()->create([
			'seller_id' => auth()->user()->id,
			'status' => 'received',
			'price' => $totalOrderPrice,
			'payment_type' => $data->payment_type
		]);

		foreach($productsPreparedToAttach as $key => $sizes) {
			foreach($sizes as $size) {
				$order->products()->attach($key, $size);
			}
		}

		return $order;
	}

    /**
     * @param Order $order
     * @param string $orderStatus
     * @return bool
     */
    public function updateStatus(Order $order, string $orderStatus): bool
	{	
		return $order->update([
			'status' => $orderStatus
		]);
	}
}