<?php

namespace App\Http\Repositories;

use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;

class UserRepository
{
    public function findByEmail(string $email)
    {
        return User::whereEmail($email)->with('vouchersUsedToday')->first();
    }

    public function all(): Collection
    {
        return User::all();
    }

    /**
     * @return mixed
     */
    public function allSellers()
    {
        return User::whereRole('seller')->get();
    }

    public function vouchers()
    {
        return auth()->user()->vouchersUsedToday;
    }

	public function create($data): User
	{
		$user = User::create([
			'email' => $data['email'],
			'name' => ucwords($data['name']),
			'role' => 'seller',
			'password' => Hash::make($data['password']),
			'status' => 'active'
		]);

		return $user;
	}

	public function update(User $user, $data): bool
	{
	    if (array_key_exists('password', $data)) {
            return $user->update([
                'name' => ucwords($data['name']),
                'email' => $data['email'],
                'password' => Hash::make($data['password'])
            ]);
        }

        return $user->update([
            'name' => ucwords($data['name']),
            'email' => $data['email'],
        ]);
	}

    /**
     * @param User $user
     * @param $data
     * @return bool
     */
    public function updateSelf(User $user, $data): bool
    {
        return $user->update([
            'password' => Hash::make($data['password'])
        ]);
    }

    /**
     * @param User $user
     * @return bool|null
     * @throws \Exception
     */
    public function softDelete(User $user): ?bool
    {
        return $user->delete();
    }

    /**
     * @param User $user
     * @return bool|null
     */
    public function forceDelete(User $user): ?bool
    {
        return $user->forceDelete();
    }
}
