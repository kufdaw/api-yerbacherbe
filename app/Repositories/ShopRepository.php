<?php

namespace App\Http\Repositories;

use App\Models\Shop;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ShopRepository
{
    /**
     * @return Shop[]|Collection
     */
    public function all()
    {
        return Shop::all();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        $shop = Shop::create([
            'name' => $data['name'],
            'coordinate_x' => $data['coordinate_x'],
            'coordinate_y' => $data['coordinate_y'],
            'street' => $data['street'],
            'city' => $data['city'],
            'postal_code' => $data['postal_code']
        ]);

        $shop->managers()->sync(Arr::pluck($data['managers'], 'id'));
        $shop->sellers()->sync(Arr::pluck($data['sellers'], 'id'));

        return $shop;
    }

    /**
     * @param Shop $shop
     * @param array $data
     * @return bool
     */
    public function update(Shop $shop, array $data): bool
    {
        $shop->managers()->sync(Arr::pluck($data['managers'], 'id'));
        $shop->sellers()->sync(Arr::pluck($data['sellers'], 'id'));

        return $shop->update([
            'name' => $data['name'],
            'street' => $data['street'],
            'city' => $data['city'],
            'postal_code' => $data['postal_code'],
            'coordinate_x' => $data['coordinate_x'],
            'coordinate_y' => $data['coordinate_y'],
        ]);
    }

    /**
     * @param Shop $shop
     * @return bool|null
     * @throws \Exception
     */
    public function delete(Shop $shop): ?bool
    {
        return $shop->delete();
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function whereUserIsSeller(User $user)
    {
        return Shop::whereHas('sellers', function ($query) use ($user) {
            $query->where('user_id', $user->id);
        })->with(['managers', 'sellers'])->get();
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function whereUserIsManager(User $user)
    {
        return Shop::whereHas('managers', function ($query) use ($user) {
            $query->where('user_id', $user->id);
        })->with(['managers', 'sellers'])->get();
    }
}
