<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'name', 'desc'
    ];

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    public function sizes() {
        return $this->hasMany('App\Models\ProductSize');
    }
}
