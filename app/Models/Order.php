<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Product;
use App\Models\OrderItem;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Order extends Model
{
    
    protected $fillable = [
        'seller_id', 'status', 'price', 'payment_type', 'shop_id'
    ];

    /**
     * @return Builder
     */
    public function seller(): Builder
    {   
        $user = $this->belongsTo(User::class);

        return $user->getQuery()->where('role', 'seller');
    }

    /**
     * @return BelongsToMany
     */
    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'order_items')->withPivot(['product_size_id', 'amount'])->using(OrderItem::class);
    }

    /**
     * @return BelongsTo
     */
    public function shop(): BelongsTo
    {
        return $this->belongsTo(Shop::class);
    }
}
