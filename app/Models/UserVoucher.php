<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserVoucher extends Model
{
    protected $table = 'user_voucher';

    protected $fillable = [
        'user_id', 'order_id'
    ];
}
