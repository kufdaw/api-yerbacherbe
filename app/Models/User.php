<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, SoftDeletes;

    private const ROLE_ADMIN = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'role', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return HasMany
     */
    public function orders(): HasMany
    {
        return $this->hasMany(Order::class, 'seller_id');
    }

    /**
     * @return HasMany
     */
    public function voucher(): HasMany
    {
        return $this->hasMany(UserVoucher::class);
    }

    /**
     * @return HasMany|Builder
     */
    public function vouchersUsedToday()
    {
        return $this->voucher()->whereDate('created_at', Carbon::today());
    }

    /**
     * @return BelongsToMany
     */
    public function shopsIsSeller(): BelongsToMany
    {
        return $this->belongsToMany(Shop::class, 'shops_sellers', 'user_id', 'shop_id');
    }

    /**
     * @return BelongsToMany
     */
    public function shopsIsManager(): BelongsToMany
    {
        return $this->belongsToMany(Shop::class, 'shops_managers', 'user_id', 'shop_id');
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->role === self::ROLE_ADMIN;
    }
}
