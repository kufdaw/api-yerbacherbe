<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use App\Models\ProductSize;

class OrderItem extends Pivot
{
    protected $fillable = [
        'order_id', 'product_id', 'product_size_id', 'amount'
    ];

    public function size()
    {
        return $this->belongsTo(ProductSize::class, 'product_size_id');
    }
}
