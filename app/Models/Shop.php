<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Shop extends Model
{
    protected $fillable = [
        'name', 'coordinate_x', 'coordinate_y', 'street', 'city', 'postal_code'
    ];

    /**
     * @return HasMany
     */
    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }

    /**
     * @return BelongsToMany
     */
    public function managers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'shops_managers', 'shop_id', 'user_id');
    }

    /**
     * @return BelongsToMany
     */
    public function sellers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'shops_sellers', 'shop_id', 'user_id');
    }
}
