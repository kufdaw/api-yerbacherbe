<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateSelfUserRequest;
use App\Http\Resources\Dashboard\UserCollection;
use App\Http\Resources\Dashboard\UserResource;
use App\Models\Shop;
use App\Models\User;
use App\Services\User\UserCreator;
use App\Services\User\UserDeleter;
use App\Services\User\UserFetcher;
use App\Services\User\UserUpdater;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\DeleteUserRequest;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
	protected $userFetcher;
	protected $userCreator;
	protected $userUpdater;
	protected $userDeleter;
	protected $shopFetcher;

	public function __construct(
	    UserFetcher $userFetcher,
        UserCreator $userCreator,
        UserUpdater $userUpdater,
        UserDeleter $userDeleter
    ) {
	    $this->userFetcher = $userFetcher;
	    $this->userCreator = $userCreator;
	    $this->userUpdater = $userUpdater;
	    $this->userDeleter = $userDeleter;
	}

    /**
     * @param User $user
     * @return UserResource
     */
    public function show(User $user): UserResource
    {
        return new UserResource($user);
    }

    /**
     * @return UserResource
     */
    public function showMe(): UserResource
    {
        return new UserResource(auth()->user());
    }

    /**
     * @param Shop $shop
     * @return UserCollection
     */
    public function indexShop(Shop $shop): UserCollection
    {
        return new UserCollection($shop->sellers);
    }

    /**
     * @return UserCollection
     */
    public function index(): UserCollection
    {
        $users = $this->userFetcher->all()->sortBy('role');

        return new UserCollection($users);
    }

    /**
     * @return UserCollection
     */
    public function indexSellers(): UserCollection
    {
        $sellers = $this->userFetcher->allSellers();

        return new UserCollection($sellers);
    }

    /**
     * @return JsonResponse
     */
    public function getVouchers(): JsonResponse
    {
        $vouchers = $this->userFetcher->getVouchers();

        return response()->json([
            'success'               => true,
            'vouchers_used_today'   => $vouchers
        ]);
	}

    /**
     * @param CreateUserRequest $request
     * @return UserResource
     */
    public function create(CreateUserRequest $request): UserResource
 	{
		 $user = $this->userCreator->create($request);

		 return new UserResource($user);
 	}

 	public function update(User $user, UpdateUserRequest $request): JsonResponse
 	{
        $this->userUpdater->update($user, $request->only(['email', 'name', 'role', 'password']));

		return response()->json([
			'success' => true,
  			'message' => 'User has been successfully updated.'
		], 200);
 	}

    /**
     * @param UpdateSelfUserRequest $request
     * @return JsonResponse
     */
    public function updateSelf(UpdateSelfUserRequest $request): JsonResponse
    {
        $this->userUpdater->updateSelf(auth()->user(), $request->only(['password']));

        return response()->json([
            'success' => true,
            'message' => 'User has been successfully updated.'
        ], 200);
    }

    /**
     * @param DeleteUserRequest $request
     * @param User $user
     * @return UserResource
     * @throws \Exception
     */
    public function delete(DeleteUserRequest $request, User $user): UserResource
    {
        $this->userDeleter->delete($user);

        return new UserResource($user);
    }
}
