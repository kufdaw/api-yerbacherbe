<?php

namespace App\Http\Controllers;

use App\Events\OrderStatusUpdated;
use App\Models\Order;
use App\Events\OrderStored;
use App\Models\Shop;
use App\Services\Order\OrderCreator;
use App\Services\Order\OrderDeleter;
use App\Services\Order\OrderFetcher;
use App\Services\Order\OrderUpdater;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\IndexHistoryRequest;
use App\Http\Requests\UpdateStatusRequest;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
    protected $orderFetcher;
    protected $orderUpdater;
    protected $orderCreator;
    protected $orderDeleter;

    public function __construct(
        OrderCreator $orderCreator,
        OrderFetcher $orderFetcher,
        OrderUpdater $orderUpdater,
        OrderDeleter $orderDeleter
    ) {
    	$this->orderCreator = $orderCreator;
    	$this->orderFetcher = $orderFetcher;
    	$this->orderUpdater = $orderUpdater;
    	$this->orderDeleter = $orderDeleter;
 	}

    /**
     * @param Shop $shop
     * @return JsonResponse
     */
    public function indexActive(Shop $shop): JsonResponse
	{
		$orders = $this->orderFetcher->indexActive($shop);

		return response()->json([
			'success' => true,
			'data' => $orders
		]);
	}

    /**
     * @param Shop $shop
     * @param IndexHistoryRequest $request
     * @return JsonResponse
     */
    public function indexHistory(Shop $shop, IndexHistoryRequest $request): JsonResponse
	{
        $groupedProducts = $this->orderFetcher->indexHistory($shop, $request->query('start_date'), $request->query('end_date'));

		return response()->json([
			'success' => true,
			'grouped_products_by_day' => $groupedProducts
		]);
	}

 	public function create(Shop $shop, CreateOrderRequest $request): JsonResponse
 	{
        $this->orderCreator->create($shop, $request);
		event(new OrderStored($shop->id));

		return response()->json([
			'success' => true,
			'message' => 'Order has been successfully created.'
		], 201);
	}

	public function updateStatus(Order $order, UpdateStatusRequest $data) {
		$this->orderUpdater->updateStatus($order, $data->status);

        event(new OrderStatusUpdated($order->shop->id));

		return response()->json([
			'success' => true, 
			'message' => 'Order status has been updated.'
		], 200);
	}

}
