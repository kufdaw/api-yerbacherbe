<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateShopRequest;
use App\Http\Requests\UpdateShopRequest;
use App\Http\Resources\Dashboard\ShopCollection;
use App\Http\Resources\Dashboard\ShopResource;
use App\Services\Shop\ShopCreator;
use App\Services\Shop\ShopDeleter;
use App\Services\Shop\ShopFetcher;
use App\Services\Shop\ShopUpdater;
use App\Models\Shop;

class ShopController extends Controller
{
    protected $shopFetcher;
    protected $shopCreator;
    protected $shopUpdater;
    protected $shopDeleter;

    public function __construct(
        ShopFetcher $shopFetcher,
        ShopCreator $shopCreator,
        ShopUpdater $shopUpdater,
        ShopDeleter $shopDeleter
    ) {
        $this->shopFetcher = $shopFetcher;
        $this->shopCreator = $shopCreator;
        $this->shopUpdater = $shopUpdater;
        $this->shopDeleter = $shopDeleter;
    }

    /**
     * @return ShopCollection
     */
    public function index(): ShopCollection
    {
        $shops = $this->shopFetcher->all();
        $shops->load(['managers', 'sellers']);

        return new ShopCollection($shops);
    }

    /**
     * @param Shop $shop
     * @return ShopResource
     */
    public function show(Shop $shop): ShopResource
    {
        $shop->load(['managers', 'sellers']);
        return new ShopResource($shop);
    }

    /**
     * @param CreateShopRequest $request
     * @return mixed
     */
    public function create(CreateShopRequest $request)
    {
        $shop = $this->shopCreator->create($request->all());

        return new ShopResource($shop);
    }


    /**
     * @param Shop $shop
     * @param UpdateShopRequest $request
     * @return mixed
     */
    public function update(Shop $shop, UpdateShopRequest $request) {
        $this->shopUpdater->update($shop, $request->all());
        $shop->fresh();

        return new ShopResource($shop);
    }

//    /**
//     * @param Shop $shop
//     * @return bool|null
//     * @throws \Exception
//     */
//    public function delete(Shop $shop): ?bool
//    {
//        return $this->shopDeleter->delete($shop);
//    }
}
