<?php

namespace App\Http\Controllers;

use App\Services\Product\ProductFetcher;
use Illuminate\Http\JsonResponse;

class ProductController extends Controller
{
    protected $productFetcher;

	public function __construct(ProductFetcher $productFetcher)
	{
        $this->productFetcher = $productFetcher;
    }

	public function index(): JsonResponse
	{
        $products = $this->productFetcher->all();

        return response()->json([
            'success' => true,
            'data' => $products
        ]);
	}
}