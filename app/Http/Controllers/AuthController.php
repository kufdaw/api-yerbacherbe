<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginUserRequest;
use App\Services\Shop\ShopFetcher;
use App\Services\User\UserFetcher;
use Illuminate\Support\Facades\Hash;
use Zend\Diactoros\Request;

class AuthController extends Controller
{
    protected $userFetcher;
    protected $shopFetcher;

    /**
     * AuthController constructor.
     * @param UserFetcher $userFetcher
     * @param ShopFetcher $shopFetcher
     */
    public function __construct(
        UserFetcher $userFetcher,
        ShopFetcher $shopFetcher
    ) {
        $this->userFetcher = $userFetcher;
        $this->shopFetcher = $shopFetcher;
    }

    public function login(LoginUserRequest $request)
    {
        $user = $this->userFetcher->findByEmail($request->email);

        if ($user && Hash::check($request->password, $user->password) && $user->status !== 'inactive') {
            $token = $user->createToken('Laravel Password Grant Client');
            $accessToken = $token->accessToken;
            $refreshToken = $token->refreshToken;
            $user->shops_as_manager = $this->shopFetcher->whereUserIsManager($user);
            $user->shops_as_seller = $this->shopFetcher->whereUserIsSeller($user);

            return response()->json([
                'token' => $accessToken,
                'refresh_token' => $refreshToken,
                'user'  => $user
            ], 200);
        }

        return response([
            'error' => 'You have passed wrong credentials.'
        ], 422);
    }

    public function logout(Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();

        $response = 'You have been succesfully logged out!';
        return response($response, 200);
    }
}
