<?php

namespace App\Http\Requests;

use App\Rules\AllowedDayDifference;
use Illuminate\Foundation\Http\FormRequest;

class IndexHistoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date' => ['date_format:m-d-Y', new AllowedDayDifference(14)],
            'end_date' => 'date_format:m-d-Y'
        ];
    }
}
