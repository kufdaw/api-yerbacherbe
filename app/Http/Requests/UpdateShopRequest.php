<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateShopRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'street' => 'required|string',
            'city' => 'required|string',
            'postal_code' => 'required|string',
            'coordinate_x' => ['nullable', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'coordinate_y' => ['nullable', 'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
            'managers' => 'array',
            'sellers' => 'array',
            'managers.*.id' => 'exists:users,id',
            'sellers.*.id' => 'exists:users,id'
        ];
    }

    public function messages()
    {
        return [
            'coordinate_x.regex' => 'Wrong coordinates format.',
            'coordinate_y.regex' => 'Wrong coordinates format.'
        ];
    }
}