<?php

namespace App\Http\Requests;

use App\Rules\CanUseVoucher;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CreateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payment_type' => [
                'required',
                Rule::in(['terminal', 'cash', 'free', 'invalid']),
                new CanUseVoucher()
            ],
            'products' => ['required', 'array'],
            'products.*.amount' => 'required|integer|min:1',
            'products.*.sizes.*.price' => 'required|integer|min:1'
        ];
    }
}
