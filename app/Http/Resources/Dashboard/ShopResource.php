<?php

namespace App\Http\Resources\Dashboard;

use Illuminate\Http\Resources\Json\JsonResource;

class ShopResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'coordinate_x'  => $this->coordinate_x,
            'coordinate_y'  => $this->coordinate_y,
            'street'        => $this->street,
            'city'          => $this->city,
            'postal_code'   => $this->postal_code,
            'created_at'    => $this->created_at,
            'managers'      => new ManagerCollection($this->managers),
            'sellers'       => new SellerCollection($this->sellers)
        ];
    }
}