<?php

namespace App\Policies;

use App\Models\Order;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @param Order $order
     * @return bool
     */
    public function updateStatus(User $user, Order $order): bool
    {
        return $this->isAdminOrManagerOrSeller($user, $order);
    }

    /**
     * @param User $user
     * @param Order $order
     * @return bool
     */
    protected function isAdminOrManagerOrSeller(User $user, Order $order): bool
    {
        return $user->shopsIsSeller->contains('id', $order->shop->id) || $user->shopsIsManager->contains('id', $order->shop->id) || $user->isAdmin();
    }
}
