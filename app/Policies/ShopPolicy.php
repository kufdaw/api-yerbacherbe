<?php

namespace App\Policies;

use App\Models\Shop;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Log;

class ShopPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @param Shop $shop
     * @return bool
     */
    public function viewHistory(User $user, Shop $shop): bool
    {
        return $user->shopsIsManager->contains('id', $shop->id) || $user->isAdmin();
    }

    /**
     * @param User $user
     * @param Shop $shop
     * @return bool
     */
    public  function create(User $user, Shop $shop): bool
    {
        return $this->isAdminOrManagerOrSeller($user, $shop);
    }

    /**
     * @param User $user
     * @param Shop $shop
     * @return bool
     */
    public function showOrders(User $user, Shop $shop): bool
    {
        return $this->isAdminOrManagerOrSeller($user, $shop);
    }

    /**
     * @param User $user
     * @param Shop $shop
     * @return bool
     */
    public function showUsers(User $user, Shop $shop): bool
    {
        return $this->isAdminOrManagerOrSeller($user, $shop);
    }

    /**
     * @param User $user
     * @param Shop $shop
     * @return bool
     */
    public function showShopData(User $user, Shop $shop): bool
    {
        return $this->isAdminOrManagerOrSeller($user, $shop);
    }

    /**
     * @param User $user
     * @param Shop $shop
     * @return bool
     */
    protected function isAdminOrManagerOrSeller(User $user, Shop $shop): bool
    {
        Log::debug($shop);
        return $user->shopsIsSeller->contains('id', $shop->id) || $user->shopsIsManager->contains('id', $shop->id) || $user->isAdmin();
    }
}
