<?php

namespace App\Services\Order;

use App\Http\Repositories\OrderRepository;
use App\Http\Repositories\ProductRepository;
use App\Models\Shop;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class OrderFetcher
{
    protected $orderRepository;
    protected $productRepository;

    /**
     * OrderFetcher constructor.
     * @param OrderRepository $orderRepository
     * @param ProductRepository $productRepository
     */
    public function __construct(
        OrderRepository $orderRepository,
        ProductRepository $productRepository
    ) {
        $this->orderRepository = $orderRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * @param Shop $shop
     * @return Collection
     */
    public function indexActive(Shop $shop): Collection
    {
        $orders = $this->orderRepository->indexActive($shop);
        $this->changeProductSizeAndAmountStructureLevel($orders);
        $this->unsetPivotKeyFromProductsCollection($orders);

        return $orders;
    }

    /**
     * @param Shop $shop
     * @param $startDate
     * @param $endDate
     * @return array
     */
    public function indexHistory(Shop $shop, $startDate, $endDate): array
    {
        $startDate = Carbon::createFromFormat('m-d-Y', $startDate)->startOfDay();
        $endDate = Carbon::createFromFormat('m-d-Y', $endDate)->startOfDay();

        $orders = $this->orderRepository->indexHistory($shop, $startDate, $endDate);
        if($orders->count() === 0) {
            return [];
        }

        $productsWithSizes = $this->productRepository->all();
        $ordersGroupedByDays = $this->groupOrdersByDays($orders);
        $productsAndSizesGroupedByDays = $this->groupProductsAndSizesByDays($ordersGroupedByDays, $productsWithSizes);

        return $productsAndSizesGroupedByDays;
    }

    /**
     * @param $orders
     */
    public function changeProductSizeAndAmountStructureLevel($orders): void
    {
        foreach($orders as $order) {
            foreach($order->products as $product) {
                $product['amount'] = $product->pivot->amount;
                $product['size'] = $product->pivot->size;
            }
        }
    }

    /**
     * @param $orders
     */
    public function unsetPivotKeyFromProductsCollection($orders): void
    {
        foreach($orders as $order) {
            foreach($order->products as $product)
            {
                unset($product->pivot);
            }
        }
    }

    /**
     * @param $ordersGroupedByDays
     * @param $productsWithSizes
     * @return array
     */
    public function groupProductsAndSizesByDays($ordersGroupedByDays, $productsWithSizes): array
    {
        $simplifiedProductsAndSizes = $this->getProductWithSizesBaseValues($productsWithSizes);

        foreach ($ordersGroupedByDays as $day => $orders) {
            $totalProductsStructure = $simplifiedProductsAndSizes;

            foreach($orders as $order) {
                foreach($order->products as $product) {
                    $totalProductsStructure[$product->name][$product->pivot->size->name] += $product->pivot->amount;
                }
            }
            $totalSoldProductsByDay[] = [
                'date' => $day,
                'products' => $totalProductsStructure
            ];
        }

        return $totalSoldProductsByDay;
    }

    /**
     * @param $products
     * @return mixed
     */
    public function getProductWithSizesBaseValues($products)
    {
        $simplifiedProductsWithSizes = $this->simplifyProductsWithSizes($products);
        $productsWithSizesWithBasicValues = $this->fillProductSizesWithBasicValues($simplifiedProductsWithSizes);
        return $productsWithSizesWithBasicValues;
    }

    /**
     * @param $products
     * @return mixed
     */
    public function simplifyProductsWithSizes($products)
    {
        return $products->map(function ($product) {
            return [
                'name' => $product->name,
                'sizes' => $product->sizes->pluck('name')
            ];
        })->pluck('sizes', 'name')->toArray();
    }

    /**
     * @param $products
     * @return mixed
     */
    public function fillProductSizesWithBasicValues($products)
    {
        foreach ($products as $product => &$sizes) {
            $sizes = array_map(function() {
                return 0;
            }, array_flip($sizes));
        }
        return $products;
    }

    /**
     * @param $orders
     * @return mixed
     */
    public function groupOrdersByDays($orders)
    {
        return $orders->groupBy(function ($order) {
            return Carbon::parse($order->created_at)->format('Y-m-d');
        });
    }
}