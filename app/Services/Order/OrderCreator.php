<?php

namespace App\Services\Order;

use App\Http\Repositories\OrderRepository;
use App\Models\Order;
use App\Models\Shop;
use Illuminate\Database\Eloquent\Model;

class OrderCreator
{
    protected $orderRepository;

    /**
     * OrderCreator constructor.
     * @param OrderRepository $orderRepository
     */
    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param Shop $shop
     * @param $data
     * @return Model
     */
    public function create(Shop $shop, $data): Model
    {
        $totalOrderPrice = $this->calcTotalOrderPrice($data->products);
        $productsPreparedToAttach = $this->prepareProductsToAttach($data->products);
        $order = $this->orderRepository->create($shop, $data, $totalOrderPrice, $productsPreparedToAttach);

        if ($data->payment_type === 'free') {
            auth()->user()->voucher()->create([
                'order_id' => $order->id
            ]);
        }

        return $order;
    }

    /**
     * @param $products
     * @return float|int
     */
    public function calcTotalOrderPrice($products)
    {
        $price = 0;
        foreach($products as $product) {
            $price += $product['amount'] * $product['sizes'][0]['price'];
        }
        return $price;
    }

    /**
     * @param $products
     * @return array
     */
    public function prepareProductsToAttach($products): array
    {
        $preparedProducts = [];
        foreach($products as $product) {
            $preparedProducts[$product['id']][] = [
                'product_size_id' => $product['sizes'][0]['id'],
                'amount' => $product['amount']
            ];
        }

        return $preparedProducts;
    }
}
