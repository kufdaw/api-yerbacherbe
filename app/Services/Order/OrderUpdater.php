<?php

namespace App\Services\Order;

use App\Http\Repositories\OrderRepository;
use App\Models\Order;
use App\Models\Shop;

class OrderUpdater
{
    protected $orderRepository;

    /**
     * OrderUpdater constructor.
     * @param OrderRepository $orderRepository
     */
    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param Order $order
     * @param string $orderStatus
     * @return bool
     */
    public function updateStatus(Order $order, string $orderStatus): bool
    {
        return $this->orderRepository->updateStatus($order, $orderStatus);
    }
}