<?php

namespace App\Services\Order;

use App\Http\Repositories\OrderRepository;

class OrderDeleter
{
    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

}