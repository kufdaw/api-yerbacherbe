<?php

namespace App\Services\User;

use App\Http\Repositories\UserRepository;
use App\Models\User;

class UserDeleter
{
    protected $userRepository;

    /**
     * UserDeleter constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param User $user
     * @return bool|null
     * @throws \Exception
     */
    public function delete(User $user): ?bool
    {
        if ($this->recognizeIfShouldBeSoftDeleted($user)) {
            return $this->userRepository->softDelete($user);
        }

        return $this->userRepository->forceDelete($user);
    }

    /**
     * @param $user
     * @return bool
     */
    protected function recognizeIfShouldBeSoftDeleted($user): bool
    {
        if ($user->orders->isNotEmpty()) {
            return true;
        }
        return false;
    }
}