<?php

namespace App\Services\User;

use App\Http\Repositories\UserRepository;
use App\Models\User;

class UserUpdater
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param User $user
     * @param $data
     * @return bool
     */
    public function update(User $user, $data): bool
    {
        return $this->userRepository->update($user, $data);
    }

    /**
     * @param User $user
     * @param $data
     * @return bool
     */
    public function updateSelf(User $user, $data): bool
    {
        return $this->userRepository->updateSelf($user, $data);
    }
}
