<?php

namespace App\Services\User;

use App\Http\Repositories\UserRepository;
use App\Models\User;

class UserCreator
{
    protected $userRepository;

    /**
     * UserCreator constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param $data
     * @return User
     */
    public function create($data): User
    {
        return $this->userRepository->create($data);
    }

}