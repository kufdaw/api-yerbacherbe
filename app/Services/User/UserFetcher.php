<?php

namespace App\Services\User;

use App\Http\Repositories\UserRepository;
use Illuminate\Support\Collection;

class UserFetcher
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->userRepository->all();
    }

    /**
     * @return mixed
     */
    public function allManagers()
    {
        return $this->userRepository->allManagers();
    }

    /**
     * @return mixed
     */
    public function allSellers()
    {
        return $this->userRepository->allSellers();
    }

    /**
     * @return mixed
     */
    public function getVouchers()
    {
        return $this->userRepository->vouchers();
    }

    /**
     * @param string $email
     * @return mixed
     */
    public function findByEmail(string $email)
    {
        return $this->userRepository->findByEmail($email);
    }
}