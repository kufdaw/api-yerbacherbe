<?php

namespace App\Services\Shop;

use App\Http\Repositories\ShopRepository;
use App\Models\Shop;

class ShopDeleter
{
    protected $shopRepository;

    public function __construct(ShopRepository $shopRepository)
    {
        $this->shopRepository = $shopRepository;
    }

    /**
     * @param Shop $shop
     * @return bool|null
     * @throws \Exception
     */
    public function delete(Shop $shop): ?bool
    {
        return $this->shopRepository->delete($shop);
    }
}