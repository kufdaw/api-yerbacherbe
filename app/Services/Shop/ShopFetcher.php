<?php

namespace App\Services\Shop;

use App\Http\Repositories\ShopRepository;
use App\Models\Shop;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

class ShopFetcher
{
    protected $shopRepository;

    public function __construct(ShopRepository $shopRepository)
    {
        $this->shopRepository = $shopRepository;
    }

    /**
     * @return Shop[]|Collection
     */
    public function all()
    {
        return $this->shopRepository->all();
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function whereUserIsSeller(User $user)
    {
        return $this->shopRepository->whereUserIsSeller($user);
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function whereUserIsManager(User $user)
    {
        return $this->shopRepository->whereUserIsManager($user);
    }
}