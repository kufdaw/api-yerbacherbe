<?php

namespace App\Services\Shop;

use App\Http\Repositories\ShopRepository;
use App\Models\Shop;

class ShopUpdater
{
    protected $shopRepository;

    public function __construct(ShopRepository $shopRepository)
    {
        $this->shopRepository = $shopRepository;
    }

    /**
     * @param Shop $shop
     * @param array $data
     * @return bool
     */
    public function update(Shop $shop, array $data): bool
    {
        return $this->shopRepository->update($shop, $data);
    }
}