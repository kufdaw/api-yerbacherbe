<?php

namespace App\Services\Shop;

use App\Http\Repositories\ShopRepository;

class ShopCreator
{
    protected $shopRepository;

    /**
     * ShopCreator constructor.
     * @param ShopRepository $shopRepository
     */
    public function __construct(ShopRepository $shopRepository)
    {
        $this->shopRepository = $shopRepository;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->shopRepository->create($data);
    }
}