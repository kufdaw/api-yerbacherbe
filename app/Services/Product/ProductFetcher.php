<?php

namespace App\Services\Product;

use App\Http\Repositories\ProductRepository;
use Illuminate\Database\Eloquent\Collection;

class ProductFetcher
{
    protected $productRepository;

    /**
     * ProductFetcher constructor.
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }


    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->productRepository->all();
    }

}