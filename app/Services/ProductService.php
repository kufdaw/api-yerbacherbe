<?php

namespace App\Http\Services; 

use App\Models\Product;
use App\Http\Repositories\ProductRepository;
use Illuminate\Database\Eloquent\Collection;

class ProductService {

	private $productRepository;

	public function __construct(ProductRepository $productRepository)
	{
		$this->productRepository = $productRepository;
	}

	public function index(): Collection
	{
		$products = $this->productRepository->all();

		$this->addAmountAttributeToProduct($products);

		return $products;
	}

	public function create($data): Product
	{
		return $this->productRepository->create($data);
	}

	public function update(Product $product, $data): bool
	{
		return $this->productRepository->update($product, $data);
	}

	public function addAmountAttributeToProduct(&$products)
	{
		foreach($products as $product) {
			$product['amount'] = 1;
		}
	}
}