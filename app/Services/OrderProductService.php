<?php

namespace App\Http\Services;

use App\Models\OrderProduct;
use App\Http\Repositories\OrderProductRepository;

class OrderProductService {
    private $orderProductRepository;

    public function __construct(OrderProductRepository $orderProductRepository)
    {
        $this->orderProductRepository = $orderProductRepository;
    }

    public function create(Order $order, Product $product, $data): OrderProduct
    {
        return $this->orderProductRepository->create($order, $product, $data);
    }

    public function update(Order $order, Product $product, $data): bool
    {
        return $this->orderProductRepository->update($order, $product, $data);
    }

    public function delete(Order $order, Product $product): bool
    {
        return $this->orderProductRepository->delete($order, $product);
    }
}