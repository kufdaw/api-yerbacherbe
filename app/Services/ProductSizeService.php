<?php

namespace App\Http\Services;

use App\Http\Repositories\ProductSizeRepository;

class ProductSizeService {

    private $productSizeRepository;

    public function __construct(ProductSizeRepository $productSizeRepository)
    {
        $this->productSizeRepository = $productSizeRepository;
    }
    
}