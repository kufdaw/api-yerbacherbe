<?php

use App\Http\Controllers\ProductController;
use App\Http\Middleware\AdminMiddleware;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['json.response']], function () {
    Route::post('/login', 'AuthController@login');

    Route::middleware('auth:api')->group(function () {
        Route::get('/vouchers', 'UserController@getVouchers');

        Route::get('/products', 'ProductController@index');

        Route::get('/shops/{shop}/orders/active', 'OrderController@indexActive')->middleware('can:showOrders,shop');
        Route::post('/shops/{shop}/orders', 'OrderController@create')->middleware('can:create,shop');
        Route::put('/orders/{order}', 'OrderController@updateStatus')->middleware('can:updateStatus,order');

        Route::put('/users/update-self', 'UserController@updateSelf');
        Route::get('/users/me', 'UserController@showMe');

        Route::get('/shops/{shop}/users', 'UserController@indexShop')->middleware('can:showUsers,shop');
        Route::get('/shops/{shop}', 'ShopController@show')->middleware('can:showShopData,shop');
        Route::get('/shops/{shop}/orders/history', 'OrderController@indexHistory')->middleware('can:viewHistory,shop');

        Route::group(['middleware' => AdminMiddleware::class], function() {
            Route::get('/users/sellers', 'UserController@indexSellers');
            Route::get('/users/{user}', 'UserController@show');
            Route::get('/users', 'UserController@index');
            Route::post('/users', 'UserController@create');
            Route::put('/users/{user}', 'UserController@update');
            Route::delete('/users/{user}', 'UserController@delete');

            Route::get('/shops', 'ShopController@index');
            Route::post('/shops', 'ShopController@create');
            Route::put('/shops/{shop}', 'ShopController@update');
        });
    });
});
